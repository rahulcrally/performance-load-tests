param(
    [Parameter(Mandatory=$True)]
    [string]$NugetApiKey
)

# Get currently deployed release version
pushd .seagull
. ".\OctopusInfo.ps1"

$releaseVersions = GetOctoLatestDeployed "$env:TF_VAR_environment"

$projectName = Get-OctopusProject "$env:ProjectName" 
$appVersion = ($releaseVersions | ? {$_.ProjectID -eq $projectName.Id}).ReleaseVersion

# Get corresponding version for Gatling tests
popd

curl https://rallyhealth.myget.org/F/marketplace-common/auth/$NugetApiKey/api/v2/package/$env:ProjectName.Gatling/$appVersion -OutFile gatling.zip

Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip ".\gatling.zip" ".\gatling"