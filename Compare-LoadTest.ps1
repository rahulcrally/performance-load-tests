param(
    [string] $BaseLineUUID,
    [string[]] $CompareToUUIDs,
    [string] $Environment,
    [int] $NumberOfLastFloods = 1,
    [string] $DataDogAPI,
    [string] $DataDogAPP,
    [string] $FloodAPI
)

Function Get-DataDogEndPointMetrics($StartedUnixEpocTime, $EndUnixEpocTime)
{
    $ddapi = "api_key=$DataDogAPI"
    $ddapp = "application_key=$DataDogAPP"
    $query = "sum:Choice.HttpApi.count{environment:loadtest}by{endpoint}.as_count()"

    $data = Invoke-RestMethod -Uri "https://app.datadoghq.com/api/v1/query?$($ddapp)&$($ddapi)&from=$StartedUnixEpocTime&to=$EndUnixEpocTime&query=$query"

    $endpointlookup = @{}
    foreach ($datapoint in $($data.series))
    {
        $total = 0
        $pstable = $null
        $row = $null
        $name = $null
        $timetable = @{}
        $datapoint.pointlist | foreach {$timetable[$_.item(0)] = $_.item(1); $total += $_.item(1)}
        
        $name = $datapoint.scope.split(',')[0].replace("endpoint:","").trim()
        
        $row = @{DetailsbyTime=$timetable;
            TotalCalls=$([system.math]::Round($total))}

        $PSTable = New-Object -TypeName PSObject -Property $row
        $endpointlookup[$name] = $pstable
    }

    return $endpointlookup
}

Function Get-FloodIDs($baseuri, $floodHeader, $top, $env)
{
    $info = Invoke-RestMethod -URI "$($baseuri)/floods" -Headers $floodHeader
    $floodinfo = $info._embedded.floods | where {$_.name -match "Choice" -and $_.tag_list -match "$env" -and $_.tag_list -contains "comparable:true"} | Sort-Object started -Descending -Top $top

    return $floodinfo
}

Write-Host "Start of script"

$FloodAPI = "https://api.flood.io"
$userid = "FloodAPI"
$password = ""
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($userid):$password"))
$AuthHeader = @{Authorization="Basic $base64AuthInfo"}
$OutputTable = @()

if(!$CompareToUUIDs)
{
    Write-Host "Finding other loadtests to compare with"
    $floodinfo = Get-FloodIDs $FloodAPI $AuthHeader $NumberOfLastFloods $Environment
    $CompareToUUIDs = $floodinfo | select -ExpandProperty uuid
}

if($CompareToUUIDs.count -lt 1)
{
    Write-Host "FAILED: To find any loadtests to Compare against, please manual specify"
    $exitcode = 100
}
else
{
    Write-Host "Fetching Baseline information from Flood"
    $BaseFlood = Invoke-RestMethod -URI "$($FloodAPI)/floods/$BaseLineUUID" -Headers $AuthHeader

    if(!$BaseFlood)
    {
        Write-Host "FAILED: Finding Baseline Flood Info"
        $exitcode = 1
    }
    else
    {
        $StartedUnixEpocTime = (New-TimeSpan -Start $(Get-Date -Date "01/01/1970") -End $(get-date $($BaseFlood.started.replace(' UTC','-5')))).TotalSeconds
        $EndUnixEpocTime = (New-TimeSpan -Start $(Get-Date -Date "01/01/1970") -End $(get-date $($BaseFlood.stopped.replace(' UTC','-5')))).TotalSeconds
        $BaselineEndpointResults = Get-DataDogEndPointMetrics $StartedUnixEpocTime $EndUnixEpocTime

        Write-Host "Starting to Build Final Table from Baseline Data"
        foreach ($key in $BaselineEndpointResults.keys)
        {
            $basecount = $BaselineEndpointResults[$key].TotalCalls
            $outputrow = @{name = $key; baselinecount = $basecount}
            $pstable = New-Object -TypeName pscustomobject -Property $outputrow
            
            $OutputTable += $pstable | select name,baselinecount
        }

        foreach ($floodcompare in $CompareToUUIDs)
        {
            $Flood = Invoke-RestMethod -URI "$($FloodAPI)/floods/$floodcompare" -Headers $AuthHeader
            $FloodFriendlyName = $Flood.tag_list | where {$_ -match "name:"}

            if(!$FloodFriendlyName)
            {
                Write-Host "No Friendly Name Found using timestamp"
                $FloodFriendlyName = get-date $($flood.started.replace(' UTC','-0')) -Format "G"
            }
            else
            {
                $FloodFriendlyName = $FloodFriendlyName.split(':')[1]
            }

            $StartedUnixEpocTime = (New-TimeSpan -Start $(Get-Date -Date "01/01/1970") -End $(get-date $($flood.started.replace(' UTC','-5')))).TotalSeconds
            $EndUnixEpocTime = (New-TimeSpan -Start $(Get-Date -Date "01/01/1970") -End $(get-date $($flood.stopped.replace(' UTC','-5')))).TotalSeconds
            $EndpointResults = Get-DataDogEndPointMetrics $StartedUnixEpocTime $EndUnixEpocTime

            #TODO: This is def a slow way to do this, but for now going with path of least resistance
            $TotalCountExpression = @{Name="$FloodFriendlyName"; Expression = {$EndpointResults[$_.name].TotalCalls}}
            $CountDiffExpression = @{Name="$FloodFriendlyName-diff"; Expression = {$EndpointResults[$_.name].TotalCalls - $_.baselinecount}}
            
            $OutputTable = $OutputTable | select *, $TotalCountExpression, $CountDiffExpression
            
            return $OutputTable
        }
    }
}

exit $exitcode

#TODO: Next Steps
#Get data from flood and be able to show improvements of each endpoint
#Get data from flood to determmine when a latency spike occurs and find which end points 
#   had increasing transactions to find end points that create bottle necks
#Build an endpoint transaction map flood call -> end point call -> to DB execution status
