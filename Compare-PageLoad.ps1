param(
    $FloodUUID,
    $HipChatAPIKey,
    $FloodApiKey
)

Function Get-ResultsData ($endpoints, $FloodUUID)
{
    $results = Invoke-RestMethod -URI "$($baseuri)/floods/$FloodUUID/results" -Headers $AuthHeader | ConvertFrom-Csv
    $filterResults = $results | where {$_.Measurement -eq "response_time"}

    $hashtable = @{}
    $bytime = @{}
    $lastvalue = @{}
    $failedarry = @()

    if(!$endpoints)
    {
        $endpoints = @()
        $filterResults.label | foreach{$endpoints += $_.split("+")[0]}
        $endpoints = $endpoints | select -Unique    
    }
    
    #init table with blank array
    foreach ($endpoint in $endpoints)
    {
        $hashtable["$endpoint"] = @()
    }

    foreach($row in $filterResults)
    {
        $split = $row.Label.split("+")
  
        $grouping = $split[0]
        $action = $split[1]
        try 
        {
            $URL = $($split[2]).replace("%2F","/").Replace("%3F","?").Replace("%3D","=").Replace("%3C","{").Replace("%3E","}")    
        }
        catch 
        {
            if($failedarry -notcontains $row.Label)
            {
                $failedarry += $row.Label
                Write-Host "FAILED: URL Cleaning for $($row.Label)"
            }
        }

        if($endpoints -contains $grouping)
        {
            $row.label = $URL
            $row.time = get-date $row.time
            $row = $row | select *,
            @{Name="Devation"; Expression = {if($lastvalue["$grouping"].value){$row.value - $lastvalue["$grouping"].value}else{"0"}}},
            @{Name="Action"; Expression ={$action}}   

            $bytime["$($row.time)"] =
            $lastvalue["$grouping"] = $row
            $hashtable["$grouping"] += $row
        }
    }

    return $hashtable
}

Write-Host "Starting Script"

$baseuri = "https://api.flood.io"
$buildTypesURI = "/floods"
$userid = $FloodApiKey
$password = ""
$hipchatcolor = "purple"
$finaltable = @()
$x = 0

$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($userid):$password"))
$AuthHeader = @{Authorization="Basic $base64AuthInfo";"Accept"="*/*"}

Write-Host "Getting Data from Floods"
$info = Invoke-RestMethod -URI "$($baseuri)$($buildTypesURI)" -Headers $AuthHeader
$releases = $info._embedded.floods | where {$_.tag_list -match "release"}
$releases | foreach {$_.started = get-date $($_.started.replace(" UTC","-0").trim())} | Sort-object started -Descending

If($FloodUUID)
{
   $currentrelease = $info._embedded.floods | where {$_.uuid -eq $FloodUUID}
}
else
{
    $currentrelease = $releases[$x]
    $x++
}

$lastrelease = $releases[$x]
$x++

if($lastrelease)
{
    $crname  = $($currentrelease.tag_list | where {$_ -match "name:"}).Replace('name:','')
    $lrname = $($lastrelease.tag_list | where {$_ -match "name:"}).Replace('name:','')
    Write-Host "Found Current Flood $crname"


    while($crname -eq $lrname -and $releases[$x])
    {
        Write-Host "Last release Name is the same as current release cycling"
        $lastrelease = $releases[$x]
        $lrname = $($lastrelease.tag_list | where {$_ -match "name:"}).Replace('name:','')
        $x++
    }
    #get CU count
    $CurrentCUs = $currentrelease._embedded.grids.instance_quantity * $currentrelease.threads
    $LastCUs =  $lastrelease._embedded.grids.instance_quantity * $lastrelease.threads

    Write-Host "Found Last Flood $lrname"
    Write-Host "$lrname ($LastCUs CUs) -> $crname ($CurrentCUs CUs)"

    Write-Host "Getting Current Release Results"
    $currentData = Get-ResultsData $endpoints $currentrelease.uuid

    $endpoints = $currentData.keys | Sort-Object

    Write-Host "Getting Last Release Results"
    $lastData = Get-ResultsData $endpoints $lastrelease.uuid

    $MessageHTML = "<b>$lrname ($LastCUs CUs) -> $crname ($CurrentCUs CUs)</b><br>`n"
    $MessageHTML += "<table>`n"
    $MessageHTML += "<tr>`n"
    $MessageHTML += "<th>Logic Grouping---------------</th>`n"
    $MessageHTML += "<th>Change-----------------------</th>`n"
    $MessageHTML += "<th>Devation(+/-)</th>`n"
    $MessageHTML += "</tr>`n"

    foreach ($endpoint in $endpoints)
    {
        #Check to make sure the Endpoint exist
        $lavg = $($lastData[$endpoint].value | measure -Average).Average
        if($lavg)
        {
            $cavg = $($currentData[$endpoint].value | measure -Average).Average
            $cdev = $($currentData[$endpoint].devation | measure -Average).Average
            $ldev = $($lastData[$endpoint].devation | measure -Average).Average

            $change = $([System.Math]::Round($cavg - $lavg))
            $dev = $([System.Math]::Round($cdev - $ldev))

            if($dev -lt 0)
            {
                $dev  = $dev * -1
            }
        }
        else 
        {
            $change = "New EndPoint"
            $dev = "NULL"    
        }

        $row = @{
            Grouping = $endpoint;
            Change = $Change;
            Devation = $dev
        }

        $pstable = New-Object -TypeName psobject -Property $row
        $finaltable += $pstable

        $MessageHTML += "<tr>`n"
        $MessageHTML += "<td>$endpoint</td>`n"
        $MessageHTML += "<td>$change</td>`n"
        $MessageHTML += "<td>$dev</td>`n"
        $MessageHTML += "</tr>`n"

        $cavg = $null
        $lavg = $null
        $cdev = $null
        $ldev = $null
        $row = $null
        $pstable = $null
    }

    $MessageHTML += "</table>"
    $finaltable | Out-Host

    if($LastCUs -ne $CurrentCUs)
    {
        $hipchatcolor = "yellow"
        $MessageHTML = "<b>CHANGE IN CUs - PLEASE REVIEW</b><br>`n" + $MessageHTML

    }
    elseif($finaltable.Change -contains "New EndPoint")
    {
        $hipchatcolor = "yellow"
        $MessageHTML = "<b>New ENDPOINT FOUND - PLEASE REVIEW</b><br>`n" + $MessageHTML
    }
    elseif($finaltable.Change -gt 500)
    {
        $hipchatcolor = "red"
    }
    elseif($finaltable.Change -lt -500)
    {
        $hipchatcolor = "green"
    }

    $hipchatPayload = @{
            "color" = "$hipchatcolor";
            "message" = "$MessageHTML";
            "notify" ="false";
            "message_format" = "html"
        }

    Invoke-RestMethod -Method POST -Uri "https://hipchat.rallyhealth.com/v2/room/312/notification?auth_token=$HipChatAPIKey" -Body $hipchatPayload
}
else
{
    Write-Host "Could not Find a Release Test to compare against"    
}