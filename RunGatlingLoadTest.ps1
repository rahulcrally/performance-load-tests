param(
    [Parameter(Mandatory=$True)]
    [string]$ApiToken,
    [Parameter(Mandatory=$True)]
    [string]$NumThreads,
    [Parameter(Mandatory=$True)]
    [string]$RampupSec,
    [Parameter(Mandatory=$True)]
    [string]$DurationSec,
    [Parameter(Mandatory=$True)]
    [string]$BuildNumber,
    [Parameter(Mandatory=$True)]
    [string]$NodeQty,
    [Parameter(Mandatory=$True)]
    [string]$GridStopMin,
    [Parameter(Mandatory=$True)]
    [string]$InstanceType,
    [Parameter(Mandatory=$True)]
    [string]$Region,
    [Parameter(Mandatory=$True)]
    [string]$AvailZone,
    [Parameter(Mandatory=$True)]
    [string]$AcceptableErrorRate
)

python get-pip.py
pip install virtualenv

virtualenv .\LoadTest
.\LoadTest\Scripts\activate

pip install -r .\requirements.txt

# TODO (YAGNI for now)
# if not tui or titanis, 
# do sbt gatling:test instead of flood

Add-Type -AssemblyName System.IO.Compression
Add-Type -AssemblyName System.IO.Compression.FileSystem

$class = @"
public class WinToUnixEncoder : System.Text.UTF8Encoding {
    public WinToUnixEncoder() : base(true) { }

    public override byte[] GetBytes(string s) {
        s = s.Replace("\\", "/");
        return base.GetBytes(s);
    }

}
"@

Add-Type -TypeDefinition $class

function Zip
{
    param([string]$dir, [string]$outpath)
    $encoder = new-object WinToUnixEncoder
    [System.IO.Compression.ZipFile]::CreateFromDirectory($dir, $outpath, [System.IO.Compression.CompressionLevel]::Fastest, $False, $encoder)
}
Zip ".\gatling\$env:ProjectName" "tests.zip"

$gatlingFile = "./gatling/FloodLoadTest.scala"
$password = $env:LOAD_TEST_USER_PASSWORD
$username = $env:LOAD_TEST_USER_NAME
$numCases = $env:CasesToGenerate
$brokerageGuid = $env:BrokerageGuid
python FloodRunner.py -a $ApiToken --RUNTIME gatling --ENVIRONMENT $env:TF_VAR_environment --AUTHNURL $env:AuthN_Url --USERNAME $username --PASSWORD $password -g $gatlingFile --GATLING_ZIP tests.zip --BROKERAGE $brokerageGuid -t $NumThreads -r $RampupSec -d $DurationSec -b $BuildNumber -q $NodeQty -s $GridStopMin -i $InstanceType -l $Region -z $AvailZone -e $AcceptableErrorRate --NUMCASES $numCases