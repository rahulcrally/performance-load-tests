import requests
import json
import argparse
import sys
import time
import traceback
from datetime import datetime

parser = argparse.ArgumentParser(description='Take values for running the load test')
parser.add_argument('-a', '--APITOKEN', nargs='?', default='', metavar='')
parser.add_argument('-t', '--NUM_THREADS', nargs='?', default='1', metavar='')
parser.add_argument('-r', '--RAMPUP_SEC', nargs='?', default='300', metavar='')
parser.add_argument('-d', '--DURATION_SEC', nargs='?', default='300', metavar='')
parser.add_argument('-j', '--JMETER_FILE', nargs='?', default='Choice_Load_Test.jmx', metavar='')
#parser.add_argument('-u', '--PERF_TEST_USERS', nargs='?', default='GenPerfTest_Users.csv', metavar='')
parser.add_argument('-p', '--ADMIN_TEST_USERS', nargs='?', default='loadTestUser.csv', metavar='')
parser.add_argument('-x', '--EXCEL_FILE', nargs='?', default='Load_Employees-1.xlsx', metavar='')
parser.add_argument('-c', '--BROKERAGES', nargs='?', default='PerfTest_Brokerages.csv', metavar='')
parser.add_argument('-b', '--BUILD_NUMBER', nargs='?', default='1', metavar='')
parser.add_argument('-q', '--NODE_QTY', nargs='?', default='1', metavar='')
parser.add_argument('-s', '--GRIDSTOP_MIN', nargs='?', default='10', metavar='')
parser.add_argument('-i', '--INSTANCE_TYPE', nargs='?', default='m4.xlarge', metavar='')
parser.add_argument('-l', '--REGION', nargs='?', default='us-east-1', metavar='')
parser.add_argument('-z', '--AVAL_ZONE', nargs='?', default='us-east-1e', metavar='')
parser.add_argument('-e', '--ACCEPTABLE_ERROR_RATE', nargs='?', default='.10', metavar='')
parser.add_argument('-m', '--SPOT_PRICE', nargs='?', default=0.58, type=float)
parser.add_argument('-g', '--GATLING_FILE', nargs='?', default='FloodLoadTest.scala', metavar='')
parser.add_argument('--GATLING_ZIP', nargs='?', default='Gatling.zip', metavar='')
parser.add_argument('--RUNTIME', nargs='?', default='python', metavar='')
parser.add_argument('--USERNAME', nargs='?', default='', metavar='')
parser.add_argument('--PASSWORD', nargs='?', default='', metavar='')
parser.add_argument('--ENVIRONMENT', nargs='?', default='loadtest', metavar='')
parser.add_argument('--AUTHNURL', nargs='?', default='https://accounts.load-test.werally.in', metavar='')
parser.add_argument('--BROKERAGE', nargs='?', default='48be7739-a1bb-4fe7-9f59-6244e182731a', metavar='')
parser.add_argument('--NUMCASES', nargs='?', default='5', metavar='')
parser.add_argument('--DESCRIPTION', nargs='?', default='this-guy', metavar='')
parser.add_argument('--USEFORCOMPARE', nargs='?', default='FALSE', metavar='')
parser.add_argument('--GRID', nargs='?', default='', metavar='')
parser.add_argument('--HIPCHATAPITOKEN', nargs='?', default='', metavar='')

args = parser.parse_args()

print str(args).replace(",","\n")

# Helpful for debugging
# print json.dumps(respJSON, indent=2, sort_keys=True)

def run_flood(canRetry, floodGrid):
    floodUUID = None
    try:
        floodFile = None
        floodPayload = None
        if args.RUNTIME != 'python':
            floodFile = {
                ("flood_files[]", open(args.GATLING_FILE, 'rb')),
                ("flood_files[]", open(args.GATLING_ZIP, 'rb'))
            }

            javaOpts = "-Dd=rallytesting.net, -De=" + args.ENVIRONMENT + ", -Da=" + args.AUTHNURL + ", -Dp=" + args.PASSWORD + ", -Du=" + args.USERNAME + ", -Dbg=" + args.BROKERAGE + ", -Dcg" + args.NUMCASES

            if floodGrid != None:
                floodPayload = {
                    "flood[tool]": "gatling",
                    "flood[threads]": args.NUM_THREADS,
                    "flood[rampup]": args.RAMPUP_SEC,
                    "flood[duration]": args.DURATION_SEC,
                    "flood[privacy_flag]": "public",
                    "flood[name]": "Gatling-" + args.BUILD_NUMBER,
                    "flood[tag_list]": "prod,api-initiated,name:" + args.DESCRIPTION + args.ENVIRONMENT,
                    "flood[override_parameters]": javaOpts,
                    "flood[grids][][uuid]": floodGrid
                }
            else:
                floodPayload = {
                    "flood[tool]": "gatling",
                    "flood[threads]": args.NUM_THREADS,
                    "flood[rampup]": args.RAMPUP_SEC,
                    "flood[duration]": args.DURATION_SEC,
                    "flood[privacy_flag]": "public",
                    "flood[name]": "Gatling-" + args.BUILD_NUMBER,
                    "flood[tag_list]": "prod,api-initiated,name:" + args.DESCRIPTION + ",env:" + args.ENVIRONMENT,
                    "flood[override_parameters]": javaOpts,
                    "flood[grids][][region]": args.REGION,
                    "flood[grids][][infrastructure]": "hosted",
                    "flood[grids][][account_credential_id]": "47",
                    "flood[grids][][instance_quantity]": args.NODE_QTY,
                    "flood[grids][][stop_after]": args.GRIDSTOP_MIN,
                    "flood[grids][][instance_type]": args.INSTANCE_TYPE,
                    "flood[grids][][aws_tags]": "environment=floodio,owner=floodio",
                    "flood[grids][][aws_availability_zone]": args.AVAL_ZONE
                }
        else:
            floodFile = {
                ("flood_files[]", open(args.JMETER_FILE, 'rb')),
                ("flood_files[]", open(args.EXCEL_FILE, 'rb')),
                ("flood_files[]", open(args.BROKERAGES, 'rb')),
                ("flood_files[]", open(args.ADMIN_TEST_USERS, 'rb'))
            }
            
            for x in range(int(args.NODE_QTY)):
                floodFile.add(("flood_files[]",open("GenPerfTest_Users_"+ str(x) +".csv", 'rb')))

            javaOpts = "-JEnvironment=" + args.ENVIRONMENT
            if not args.GRID:
                floodPayload = {
                    "flood[tool]": "jmeter-3.0",
                    "flood[threads]": args.NUM_THREADS,
                    "flood[rampup]": args.RAMPUP_SEC,
                    "flood[duration]": args.DURATION_SEC,
                    "flood[privacy_flag]": "public",
                    "flood[name]": args.JMETER_FILE + "-Python-" + args.BUILD_NUMBER,
                    "flood[tag_list]": "prod,api-initiated,name:" + args.DESCRIPTION + ",env:" + args.ENVIRONMENT,
                    "flood[override_parameters]": javaOpts,
                    "flood[grids][][region]": args.REGION,
                    "flood[grids][][infrastructure]": "hosted",
                    "flood[grids][][account_credential_id]": "47",
                    "flood[grids][][instance_quantity]": args.NODE_QTY,
                    "flood[grids][][stop_after]": args.GRIDSTOP_MIN,
                    "flood[grids][][instance_type]": args.INSTANCE_TYPE,
                    "flood[grids][][aws_tags]": "environment=floodio,owner=floodio",
                    "flood[grids][][aws_availability_zone]": args.AVAL_ZONE
                }
            else:
                floodPayload = {
                    "flood[tool]": "jmeter-3.0",
                    "flood[threads]": args.NUM_THREADS,
                    "flood[rampup]": args.RAMPUP_SEC,
                    "flood[duration]": args.DURATION_SEC,
                    "flood[privacy_flag]": "public",
                    "flood[name]": args.JMETER_FILE + "-Python-" + args.BUILD_NUMBER,
                    "flood[tag_list]": "prod,api-initiated,name:" + args.DESCRIPTION + ",env:" + args.ENVIRONMENT,
                    "flood[override_parameters]": javaOpts,
                    "flood[grids][][uuid]": args.GRID
                }

        if (args.SPOT_PRICE > 0):
            floodPayload["flood[grids][][aws_spot_price]"] = args.SPOT_PRICE

        print(floodPayload)

        # Start the flood with an associated grid
        getResponse = requests.post(url="https://api.flood.io/floods", data=floodPayload, files=floodFile, auth=(args.APITOKEN, ''))
        
        print("Flood Creation Response")
        print(str(getResponse.text).replace(",","\n"))
        
        respJSON = json.loads(getResponse.text)
        floodUUID = respJSON["uuid"]
        floodGrid = respJSON["_embedded"]["grids"][0]["uuid"]
        floodName = str(respJSON["_embedded"]["grids"][0]["name"]).replace("-production-" + floodGrid, "")
        
        print("\n\n")
        print(str(datetime.now()) + " Flood grid spinning up is: " + floodName + "\n")
        print(str(datetime.now()) + " Flood grid spinning up is: " + floodGrid + "\n")
        print(str(datetime.now()) + " Flood is waiting in queue: https://flood.io/" + floodUUID)
        print("\n\n")
        
        sys.stdout.flush()

        isLoadTestEnv = args.ENVIRONMENT == "loadtest"

        if isLoadTestEnv:
            alertHipchat(floodUUID, "Because you are wicked and perverse codebase, a plague of *" + floodName + "* will soon rain down upon your servers")

        getResponse = requests.get(url="https://api.flood.io/floods/" + floodUUID, auth=(args.APITOKEN, ''))
        respJSON = json.loads(getResponse.text)

        # The flood will not be running until the grid is spun up
        while (respJSON["status"] != "running"):
            if(respJSON["status"] == "finished"):
                raise ValueError('Test stopped prematurely.')
            print(str(datetime.now()) + " Waiting for flood to start...")
            sys.stdout.flush()
            time.sleep(60)
            getResponse = requests.get(url="https://api.flood.io/floods/" + floodUUID, auth=(args.APITOKEN, ''))
            respJSON = json.loads(getResponse.text)

        if isLoadTestEnv:
            alertHipchat(floodUUID, "In those days it came to pass that *" + floodName + "* descended upon the servers as had been prophesied, and there was weeping and gnashing of teeth")

        # continuously check to see if the test is finished and if it is erring in a significant way.
        while (respJSON["status"] != "finished"):
            concurrency = respJSON["concurrency"]
            error_rate = respJSON["error_rate"]
            print(str(datetime.now()) + " Test is running with " + str(concurrency) + " average concurrency and " + str(
                error_rate) + " error rate")
            sys.stdout.flush()

            #if (concurrency > 0 and error_rate > 0):
            #    if ((error_rate / float(concurrency)) > float(args.ACCEPTABLE_ERROR_RATE)):
            #        raise ValueError('Error rate went above acceptable levels.  Stopping the test.')

            time.sleep(60)
            getResponse = requests.get(url="https://api.flood.io/floods/" + floodUUID, auth=(args.APITOKEN, ''))
            respJSON = json.loads(getResponse.text)

        time.sleep(120)  # give flood some time to gather reporting from the grid
        getResponse = requests.get(url="https://api.flood.io/floods/" + floodUUID + "/report", auth=(args.APITOKEN, ''))
        respJSON = json.loads(getResponse.text)

        print(getResponse.text)

        if canRetry and args.RUNTIME != 'python':
            run_flood(False, floodGrid)
        else:
            with open("flood.prop", "w") as text_file:
                text_file.write("FLOOD_REPORT=" + respJSON["summary"] + " https://flood.io/" + floodUUID)
            
            if isLoadTestEnv:
                alertHipchat(floodUUID, "Test complete. Summary: " + respJSON["summary"])

    except Exception as e:
        print("ERROR: " + str(e))
        traceback.print_exc(file=sys.stdout)
        print("Stopping the test")
        sys.stdout.flush()
        getResponse = requests.get(url="https://api.flood.io/floods/" + floodUUID + "/stop", auth=(args.APITOKEN, ''))
        print(getResponse.text)
        sys.exit(-1)

def alertHipchat(floodUUID, message):
    try:
        hipchatPayload = {
            "color": "purple",
            "message": "https://flood.io/" + floodUUID + " :: " + message,
            "notify": "false",
            "message_format": "text"
        }        
        responseJson = requests.post(url="https://hipchat.rallyhealth.com/v2/room/312/notification?auth_token=" + args.HIPCHATAPITOKEN, data=hipchatPayload)        
    except Exception as e:
        print("Error posting link to hipchat: " + str(e))
        print("Continuing the test though.")
        sys.stdout.flush()

def main():
    run_flood(True, None)


if __name__ == '__main__':
    main()
