param(
    $LoadTestRunTimeSecs = 700,
    $Sleep_After_Bulk_Upload_Secs = 0,
    $environment = "loadtest",
    $UsersPerCase = 500,
    $UseAutoGen = $false,
    $RunFlood = $true,
    $FloodNodes = 10,
    $UseLoLAuth = $false,
    $FloodAPIKey,
    $HipChatAPIKey,
    $OctopusApiKey,
    $RampUpTimeSecs = 700,
    $TotalCUs =  2000,
    $RunChoiceOnly = $false,
    $BuildVersion = '1',
    $CompareName = 'choice-loadtest',
    $MedicalPercentage = 100,
    $DentalPercentage = 90,
    $VisionPercentage = 80,
    $BLADDPercentage = 80,
    $SLADDPercentage = 80
)
function Start-FloodGrid ($FloodAPIKey, $LoadTestRunTimeSecs, $Sleep_After_Bulk_Upload_Secs) 
{
    $baseuri = "https://api.flood.io"
    $buildTypesURI = "/grids"
    $userid = "$FloodAPIKey"
    $password = ""
    $gridRunTime =  [math]::Round($($([int] $LoadTestRunTimeSecs) + $([int] $Sleep_After_Bulk_Upload_Secs) + 600)/60)
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($userid):$password"))
    $AuthHeader = @{Authorization="Basic $base64AuthInfo"; "Accept"="*/*"}

    $gridinfo = @{
        "grid[region]" = "us-west-2";
        "grid[infrastructure]" = "hosted";
        "grid[account_credential_id]" = "47";
        "grid[instance_quantity]" = "$FloodNodes";
        "grid[stop_after]" = "$gridRunTime";
        "grid[instance_type]" = "m4.xlarge";
        "grid[aws_tags]" = "environment=floodio,owner=floodio";
        "grid[aws_availability_zone]" = "us-west-2c"
    }

    Write-Host "Spinning up Flood Grid for $gridRunTime mins"

    $info = Invoke-RestMethod -URI "$($baseuri)$($buildTypesURI)" -Headers $AuthHeader -Method POST -Body $gridinfo
    Write-Host "Grid $($info.uuid) is currently $($info.status)"

    return $($info.uuid)
}

function Stop-FloodGrid ($FloodAPIKey, $GridUUID)
{
    $baseuri = "https://api.flood.io"
    $userid = "$FloodAPIKey"
    $password = ""
    $return = $false

    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($userid):$password"))
    $AuthHeader = @{Authorization="Basic $base64AuthInfo"; "Accept"="*/*"}
    
    $info = Invoke-RestMethod -URI "$($baseuri)/grids/$($GridUUID)" -Headers $AuthHeader -Method Delete
    Write-Host "Grid $GridUUID is currently $($info.state)"

    if($info.state -eq "stopped")
    {
        $return = $true
    }

    return $return
}
function Get-FloodGridState ($FloodAPIKey, $GridUUID)
{
    $baseuri = "https://api.flood.io"
    $userid = "$FloodAPIKey"
    $password = ""

    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($userid):$password"))
    $AuthHeader = @{Authorization="Basic $base64AuthInfo"; "Accept"="*/*"}
    
    $info = Invoke-RestMethod -URI "$($baseuri)/grids/$($GridUUID)" -Headers $AuthHeader

    return $info.status
}

python get-pip.py
pip install virtualenv

virtualenv .\LoadTest
.\LoadTest\Scripts\activate

pip install -r .\requirements.txt

Write-Host ""
Write-Host "Starting Load Test for environment: $environment"

$CUsPerNode = $TotalCUs/$FloodNodes
$AdminUserCSV =".\loadTestUser.csv"
$brokerageCSV = ".\PerfTest_Brokerages.csv"
$AdminBulkUploadFile = ".\Load_Employees-1.xlsx"
$JMeterFile = ".\Choice_Load_Test.jmx"
$JMeterBulkUpload = ".\Choice_Upload_Census.jmx"
$GridInstanceType = "m4.xlarge"
$GridRegion = "us-west-2"
$GridZone = "us-west-2c"
$GridTimeout = 300
$SecondGridTimeout = 600
$AcceptableErrorRate = "10"
$SpotPrice = "0"
$NumberOfCases = [System.Math]::Round(((($LoadTestRunTimeSecs/240)+1)*$TotalCUs)/$UsersPerCase) #240 secs is the time it takes to run through 1 segement of users + 1 extra min (safty net)

if($RunChoiceOnly)
{
    Write-Host "Running Choice Only"
   $JMeterFile = "Choice_Only_Load_Test.jmx"
}
else 
{
    Write-Host "Running Choice and Admin"
    $JMeterFile = "Choice_Load_Test.jmx"
}

if($RunFlood)
{
    $GridUUID = Start-FloodGrid $FloodAPIKey $LoadTestRunTimeSecs $Sleep_After_Bulk_Upload_Secs
}

Write-Host "Starting Data Creation, AutoGen: $UseAutoGen"
.\Create-CasesAndEmployees.ps1 `
    -NumberOfCases $NumberOfCases `
    -NumberOfUsersPerCase $UsersPerCase `
    -environment $environment `
    -verboseoutput `
    -uselolauth:$UseLoLAuth `
    -UseAutoGen:$UseAutoGen `
    -MedicalPercentage $MedicalPercentage `
    -DentalPercentage $DentalPercentage `
    -VisionPercentage $VisionPercentage `
    -BLADDPercentage $BLADDPercentage `
    -SLADDPercentage $SLADDPercentage

python .\CSV_Split.py -n $FloodNodes

#Make Smarter
Write-Host "Sleeping for $Sleep_After_Bulk_Upload_Secs seconds"
start-sleep $([int] $Sleep_After_Bulk_Upload_Secs)

if($RunFlood)
{
    $elapsedtime = 0
    $SecondTimeOut = $GridTimeout + $SecondGridTimeout 
    while($gridstate -ne "started" -and !($timedout))
    {
        Start-Sleep 30
        $elapsedtime += 30
        if($elapsedtime -eq $GridTimeout)
        {
            Write-Host "Grid should of started by now, Stopping grid and starting a new one"
            Stop-FloodGrid $FloodAPIKey $GridUUID
            $GridUUID = Start-FloodGrid $FloodAPIKey $LoadTestRunTimeSecs 0 #sleep should of happened by now
        }
        elseif($elapsedtime -gt $SecondTimeOut)
        {
            Write-Host "Grid Still isn't starting up, ending script"
            $timedout = $true
            Stop-FloodGrid $FloodAPIKey $GridUUID
        }
        else
        {
            write-host "Waiting For Grid to finish starting up"
            $gridstate = Get-FloodGridState $FloodAPIKey $GridUUID
        }
    }
    if(!$timedout)
    {
        Write-Host "Writing Versions to Hip Chat"
        if($environment -eq "loadtest")
        {
            .\Write-VersionToHipChat.ps1 -HipChatAPIKey $HipChatAPIKey -OctopusAPIKey $OctopusApiKey
        }

        python FloodRunner.py -a $FloodAPIKey `
         -t $CUsPerNode `
         -r $RampUpTimeSecs `
         -d $LoadTestRunTimeSecs `
         -j $JMeterFile `
         -p $AdminUserCSV `
         -x $AdminBulkUploadFile `
         -c $brokerageCSV `
         -b $BuildVersion `
         -q $FloodNodes `
         -s $gridRunTime `
         -i $GridInstanceType `
         -l $GridRegion `
         -z $GridZone `
         -e $AcceptableErrorRate `
         -m $SpotPrice `
         --BUILD_NUMBER $BuildVersion `
         --ENVIRONMENT $environment `
         --DESCRIPTION $CompareName `
         --GRID $griduuid `
         --HIPCHATAPITOKEN $HipChatAPIKey 

        .\Compare-PageLoad.ps1 -HipChatAPIKey $HipChatAPIKey -FloodApiKey $FloodAPIKey
                 
        Stop-FloodGrid $FloodAPIKey $GridUUID
    }
}

if($environment -eq "loadtest")
{
    Write-Host "Removing ChoiceAdmin user"
    .\Remove-PermissionsForLoadTestUser.ps1
}