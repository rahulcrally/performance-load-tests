function Run-SqlQuery($database, $query) {
    return Invoke-SqlCmd -ServerInstance $env:DB_HOST -Database $database -Username $env:DB_USER -Password $env:DB_PWD -Query $query
}

# Get identity provider id
$identityProviderId = Run-SqlQuery Spotlite "
    SELECT IdentityProviderId 
    FROM fedgrant.IdentityProviders 
    WHERE LookupValue = '$env:LOAD_TEST_USER_ID_PROVIDER'
"
if (-not $identityProviderId) {
    Write-Host "No matching identity provider for $env:LOAD_TEST_USER_ID_PROVIDER" 
    Exit(-1)
}
$identityProviderId = $identityProviderId.Item(0)

# Check that user already exists
$userQuery = Run-SqlQuery Spotlite "
    SELECT l.LoginId, lr.LoginRoleId 
    FROM fedgrant.Logins l 
    JOIN fedgrant.LoginRoles lr on lr.LoginId = l.LoginId
    WHERE l.UPN = '$env:LOAD_TEST_USER_ID' 
    AND l.IdentityProviderId = $identityProviderId 
"

if (-not $userQuery) {
    Write-Host "User does not exist $env:LOAD_TEST_USER_ID_PROVIDER`:$env:LOAD_TEST_USER_ID"
    Exit(-1)
}

$loginId = $userQuery.Item(0)['LoginId']

foreach ($loginRole in $userQuery) {
    Run-SqlQuery Spotlite "
        DELETE fedgrant.Allowances 
        WHERE LoginRoleId = $($loginRole['LoginRoleId'])
    "

    Run-SqlQuery Spotlite "
        DELETE fedgrant.LoginRoles 
        WHERE LoginRoleId = $($loginRole['LoginRoleId'])
    "
}

Run-SqlQuery Spotlite "
    DELETE fedgrant.Logins
    WHERE LoginId = $loginId 
"

Write-Host "Removed Load Test User $env:LOAD_TEST_USER_ID_PROVIDER`:$env:LOAD_TEST_USER_ID"