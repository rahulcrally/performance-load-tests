param(
    [Parameter(Position=1)]
    $Session = $null,
    [Parameter(Position=2)]
    [switch] $EnableJobOutput = $false,
    [Parameter(Position=3)]
    [int]    $NumberOfUsersPerCase = 2,
    [Parameter(Position=4)]
    [string] $environment = "loadtest",
    [Parameter(Position=5)]
    [switch] $UseAutoGen = $false,
    [Parameter(Position=6)]
    [switch] $UseLoLAuth = $false,
    [Parameter(Position=7)]
    [string] $brokerage = "b6776c9d-31c4-4e50-aa85-8b937e85ad92",
    [Parameter(Position=8)]
    [string] $RealAuthURI = "https://accounts.load-test.werally.in",
    [Parameter(Position=9)]
    [switch] $CreateMedical = $true,
    [Parameter(Position=10)]
    [switch] $CreateVision = $false,
    [Parameter(Position=11)]
    [switch] $CreateDental = $false,
    [Parameter(Position=12)]
    [switch] $CreateBLADD = $false,
    [Parameter(Position=13)]
    [switch] $CreateSLADD = $false,
    [Parameter(Position=14)]
    [switch] $CreateLTD = $false,
    [Parameter(Position=15)]
    [switch] $CreateSTD = $false

)
<#Adding a New product Steps
Comment line can also be found on the lines that need to be adjusted
All New products should have Static and AutoGen creation, until autogen is 100% stable
1) add param for product Creation
2) Create Static Creation Function 
3) Add AutoGenProduct new products should just be param
4) Add new product into the Users csv outputm Jmeter needs to know about what products to test

More Changes in Create-LoadData.ps1
#>
function Write-Host ($msg) 
{
    if($EnableJobOutput)
    {
        "$(get-date) $msg" | Out-Host
        "$(get-date) $msg" | Out-File "./$($logguid).log" -Append
    }
}

function Set-AutoGenCase ($AdminURI, $brokerage, $mysession)
{
        $casereturn = Invoke-RestMethod -Method POST -URI $adminuri/api/brokerages/$brokerage/cases/autogen -WebSession $mysession 

        if(!$casereturn.currentCaseId)
        {
            Start-Sleep $(get-random -Maximum 10)
            Write-Host "Generating Case"
            $casereturn = Invoke-RestMethod -Method POST -URI $adminuri/api/brokerages/$brokerage/cases/autogen -WebSession $mysession
        }
        return $casereturn
}

function Set-AutoGenProduct($AdminURI, $CaseGUID, $mysession, $product, $jsonbody)
{
    $response = Invoke-RestMethod -URI $AdminURI/api/cases/$($CaseGUID)/planconfigurations/$product/autogen -Body $jsonbody -Method POST -WebSession $mysession
    if(!$response.guid)
    {
        Start-Sleep $(get-random -Maximum 10)
        $response = Invoke-RestMethod -URI $AdminURI/api/cases/$($CaseGUID)/planconfigurations/$product/autogen -Body $jsonbody -Method POST -WebSession $mysession
    }

    return $response
}

#Start of Functions for Static Creation
function Set-StaticCase($AdminURI, $brokerage, $mysession)
{
    $failedattempts = 0
    while(!$($caseresp.guid) -and $failedattempts -lt 10)
    {
        $EnrollmentStartDate = get-date -f yyyy-MM-dd
        $EnrollmentEndDate =  get-date $(get-date).AddYears(1) -f yyyy-MM-dd
        $benefitsStartDate =  get-date $(get-date).AddDays(1) -f yyyy-MM-dd
        $benefitsEndDate =  get-date $(get-date).AddDays(1).AddYears(1) -f yyyy-MM-dd
        
        $employerid = Get-Random
        $name = "$([guid]::NewGuid())"
        Write-Host "Employer ID: $employerid"
        Write-Host "Generated Name for Case: $name"

        $dependants = @(1,2)
        $payroll = @(7)
        $casebody = @{
            openEnrollmentStartDate = $EnrollmentStartDate
            openEnrollmentEndDate = $EnrollmentEndDate;
            openEnrollmentEndInstant = "$($EnrollmentEndDate)T00:00:00Z";
            benefitYearStartDate = $benefitsStartDate;
            benefitYearEndDate = $benefitsEndDate;
            payrollFrequencyIds = $payroll;
            dependentTypeIds = $dependants;
            savedDependents = $false;
            EmployerId = $employerid;
            name = $name
            address = "123 Anywhere st";
            city = "Chicago";
            statecode = "IL";
            zip = "60606";
            TimeZoneId = "Central Standard Time";
            brokerageGuid = $brokerage
        } | ConvertTo-Json

         $caseresp = Invoke-RestMethod -uri $AdminURI/api/cases/ -Method POST -Body $casebody -WebSession $mysession  -ContentType "application/json"
    
        if(!$($caseresp.guid))
        {
            Write-Host "Failed Case Generation, attempt number $failedattempts"
            Start-Sleep -Milliseconds $(get-random -Maximum 1000)
            $failedattempts++
        }
    }

    return $caseresp
}

function Set-StaticProduct ($AdminURI, $CaseGUID, $mysession, $product)
{
    $productresp = @()
    $files = Get-ChildItem  .\CreationPayload\ -Filter "*.$($product).json"
    foreach ($payload in $files)
    {
        Write-Host "Creating Product for $($payload.Name)"
        $body = Get-Content $payload.FullName -Raw | ConvertFrom-Json
        $name = "Static-$($product)-$(Get-Random)"

        if($body.caseguid)
        {
            $body.caseguid = $CaseGUID
        }

        if($body.planID)
        {
            $body.planId = $(Get-Random)
        }

        if($body.policyNumber)
        {
            $body.policyNumber = $name
        }

        if($body.PlanName)
        {
            $body.PlanName = $name
        }

        if($body.PlanDisplayName)
        {
            $body.PlanDisplayName = $name
        }
        
        if($body.validFromDate)
        {
            $body.validFromDate = get-date -f yyyy-MM-dd
        }
        
        $jsonbody  = $body | ConvertTo-Json -Depth 100

        $productresp += Invoke-RestMethod -uri $AdminURI/api/cases/$CaseGUID/planconfigurations/$product -Method POST -Body $jsonbody -WebSession $mysession -ContentType "application/json"
        
        if(!$productresp)
        {
            Start-Sleep $(get-random -Maximum 10)
            $productresp += Invoke-RestMethod -uri $AdminURI/api/cases/$CaseGUID/planconfigurations/$product -Method POST -Body $jsonbody -WebSession $mysession -ContentType "application/json"
        }
    }

    return $productresp
}

function Set-Census ($AdminURI, $CommunityGUID, $mysession, $FilePath)
{
    $fileName = Split-Path $FilePath -leaf
   
    #readfile in ReadAllBytes
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
    $CODEPAGE = "iso-8859-1" # alternatives are ASCII, UTF-8 

    # Convert byte-array to string
    $enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)

    #Encode bytes into page format
    $fileEnc = $enc.GetString($filebin)

    #create boundary
    $boundary = [System.Guid]::NewGuid().ToString()

    # Linefeed character
    $LF = "`r`n"

    #create and arry of lines with a linefeed at the end, then join the array on the feed to create a string
    $bodyLines = (
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$filename`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary--$LF"
        ) -join $LF
    
    $resp = Invoke-RestMethod -Uri "$AdminURI/api/cases/$CommunityGUID/censuses" `
    -Method Post -ContentType "multipart/form-data; boundary=`"$boundary`"" `
    -Body $bodyLines `
    -WebSession $mysession

    $cenguid = $resp.guid

    Write-Host "Census Upload $cenguid started"
    $timeout = 0
   if($cenguid)
    {
        while ($resp.status -ne 40 -and $timeout -lt 60)
        {
            Write-Host "Waiting for Census Processing to Complete"
            $resp = invoke-RestMethod -Uri "$AdminURI/api/cases/$CommunityGUID/censuses/$cenguid" -WebSession $mysession
            Start-Sleep 1
            $timeout++
        }

        if($resp.status -eq 40)
        {
            Write-Host "Committing Census Upload"
            $commitresp = invoke-RestMethod -Uri "$AdminURI/api/cases/$CommunityGUID/censuses/$cenguid/commit" `
            -Method Post `
            -WebSession $mysession
            
            if($commitresp.status -eq 70)
            {
                Write-Host "Census Commit successful"
            }
            else
            {
                Write-Host "FAILED: Committing Census"    
            }
        }
        elseif($timeout -ge 60)
        {
            Write-Host "Census upload is taking to long, moving on"
        }
        else
        {
            Write-Host "Unknown Response status from Upload"    
        }
    }
   
    return $cenguid
}

$Global:logguid = $([guid]::NewGuid())
$randomuser = "$([guid]::NewGuid())@rallyhealth.com"
$AdminURI = "https://choiceadmin.$environment.rallytesting.net"
$LoLAuth = "https://account.$environment.rallytesting.net"
$emu = "https://emu.$environment.rallytesting.net"
$scriptPath = $script:MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

$CreatedUser = 0
$EscalatedPrivs = 0
$LaunchedCase = 0
$CreatedMedical = 0
$CreatedDental = 0
$CreatedVision = 0
$CreatedBLADD = 0
$CreatedSLADD = 0
$CreatedLTD = 0
$CreatedSTD = 0
$SumofProducts = 0
$Password = $null
$LoggedIn = $false

Write-Debug "logid: $logguid"
Write-Debug "Invocation Call: $scriptpath"
Write-Debug "Invocation Directory: $dir"
Write-Debug "Current Path: $($(get-location).path)"

Set-Location $dir
Write-Host "Current Path: $($(get-location).path)"

if($Session)
{
    Write-Host "Using Existing Session"
    $mysession = $Session
    $LoggedIn = $true
}
else 
{
    #generating a random password, because there is no need for a static choiceadmin password
    for($l=40; $l -lt 122; $l++)
    {
        $ascii += ,[char][byte] $l
    }
    for($l=0; $l -lt 20; $l++)
    {
        $Password += ($ascii | Get-Random)
    }

    $Password += "1!"

    if($UseLoLAuth)
    {
        Write-Host "Registering LoLAuth User"
        $body = @{
            "email"="$randomuser";
            "password" = "$Password"
        } | convertto-json

        $CreateUser = invoke-restmethod -uri $LoLAuth/accountsstub/emaillogin -body $body -method POST -Session mysession
        $auth = $CreateUser.AuthenticationToken
    }
    else 
    {
        $loginBody = @{
            "email"= "$randomuser";
            "confirmEmail"= "$randomuser";
            "password"= "$Password";
            "confirmPassword"= "$Password";
            "acceptedToS"= $True;
            "partner"= "choiceAdmin";
            "inviteCode"= $null;
            "clientUrlSlug"= $null;
            "ssoToken"= $null;
            "credentials"= $null;
            "product"= "choiceAdmin";
            "acceptedHipaa"= $False;
            "eligFields"= @();
            "locale"= "en-US";
        } | ConvertTo-Json

        Write-Host "Real Auth Creating User"
        $loginResponse = Invoke-WebRequest -UseBasicParsing -URI "$RealAuthURI/auth/v1/register" -Method POST -Body $loginBody -Session mysession -ContentType 'application/json' 
        $auth = $($loginResponse.content | ConvertFrom-Json).authToken
    }

    if($auth)
    {
        $CreatedUser = 1
        Write-Host "Authing with Choice Admin"
        $ChoiceAdminResponse = Invoke-RestMethod -uri $adminuri/?a=$auth -method GET -WebSession $mysession

        Write-Host "Escalating Privs"
        $EscaltingPrivs = Invoke-RestMethod -uri $emu/nonprod/escalateprivileges -method GET -WebSession $mysession
        if($EscaltingPrivs.html.head.title -match "Spotlite Administration")
        {
            Write-Host "Login Successful"
            $LoggedIn = $true
        }
    }
}


if($LoggedIn)
{

    $EscalatedPrivs = 1
    Write-Host "Generating Case"
    if($UseAutoGen)
    {
        $caseinfo = Set-AutoGenCase $AdminURI $brokerage $mysession
    }
    else 
    {
        $caseinfo = Set-StaticCase $AdminURI $brokerage $mysession  
    }
    
    $CaseGUID = $caseinfo.currentCaseId
    $CommunityGUID = $caseinfo.guid
    $CaseName = $caseinfo.name

    if($CaseGUID)
    {
        Write-Host "Generated Case: $CaseGUID"   

        if($UseAutoGen)
        {
            Write-Host "Using AutoGen Endpoints"
            if($CreateMedical)
            {
                Write-Host "Generating Medical Product"
                $jsonbody = $null
                $medicalinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "medical" $jsonbody

                if($medicalinfo)
                {
                    $CreatedMedical = 1
                    $SumofProducts++
                }
            }

            if($CreateDental)
            { 
                Write-Host "Generating Dental Product"
                $jsonbody = $null
                $dentalinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "dental" $jsonbody

                if($dentalinfo)
                {
                    $CreatedDental = 1
                    $SumofProducts++
                }
            }
            
            if($CreateVision)
            {
                Write-Host "Generating Vision Product"
                $jsonbody = $null
                $visioninfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "vision" $jsonbody

                if($visioninfo)
                {
                    $CreatedVision = 1
                    $SumofProducts++
                }
            }

            if($CreateBLADD)
            {
                Write-Host "Generating Basic Life Product"
                
                $jsonbody = @{
                    includeLife = "true";
                    includeAdd = "true";
                    isEmployerPaid = "true";
                    employeeClasses = @(
                        @{
                            employeeClassId = "0";
                            name = "Employee";
                            isEligible = $true
                        }
                    )
                } | ConvertTo-Json
                $bladdinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "bladd" $jsonbody

                if($bladdinfo)
                {
                    $CreatedBLADD = 1
                    $SumofProducts++
                }
            }

            if($CreateSLADD)
            {
                Write-Host "Generating Supp Life Product"
                
                $jsonbody = @{
                    includeLife = "true";
                    includeAdd = "true";
                    isEmployerPaid = "true";
                    employeeClasses = @(
                        @{
                            employeeClassId = "0";
                            name = "Employee";
                            isEligible = $true
                        }
                    )
                } | ConvertTo-Json
                $sladdinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "sladd" $jsonbody

                if($sladdinfo)
                {
                    $CreatedSLADD = 1
                    $SumofProducts++
                }
            }
            
            if($CreateSTD)
            {
                #Comment this AutoGen Endpoint has not been created yet
                Write-Host "Generating Short Term Disablility Product"
                
                $jsonbody = ""
                $stdinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "std" $jsonbody

                if($stdinfo)
                {
                    $CreatedSTD = 1
                    $SumofProducts++
                }
            } 

            if($CreateLTD)
            {
                Write-Host "Generating Supp Life Product"
                #Comment this AutoGen Endpoint has not been created yet
                $jsonbody = ""
                $ltdinfo = Set-AutoGenProduct $AdminURI $CaseGUID $mysession "ltd" $jsonbody

                if($ltdinfo)
                {
                    $CreatedLTD = 1
                    $SumofProducts++
                }
            } 
        }
        else 
        {
            Write-Host "Using Static Generation for Products"
            if($CreateMedical)
            {
                Write-Host "Generating Medical Product"
                $medicalinfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "medical"

                if($medicalinfo)
                {
                    $CreatedMedical = 1
                    $SumofProducts++
                }
            }

            if($CreateDental)
            { 
                Write-Host "Generating Dental Product"
                $dentalinfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "dental"

                if($dentalinfo)
                {
                    $CreatedDental = 1
                    $SumofProducts++
                }
            }
            
            if($CreateVision)
            {
                Write-Host "Generating Vision Product"
                $visioninfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "vision"

                if($visioninfo)
                {
                    $CreatedVision = 1
                    $SumofProducts++
                }
            }

            if($CreateSLADD)
            {
                Write-Host "Generating Supp Life Product"
                $sladdinfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "sladd"
                
                if($sladdinfo)
                {
                    $CreatedSLADD = 1
                    $SumofProducts++
                }

            }

            if($CreateBLADD)
            {
                Write-Host "Generating Basic Life Product"
                $BladdInfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "bladd"

                if($BladdInfo)
                {
                    $CreatedBLADD = 1
                    $SumofProducts++
                }

            }
            
            if($CreateSTD)
            {
                #Comment this AutoGen Endpoint has not been created yet
                Write-Host "Generating Short Term Disablility Product"
                
                $stdinfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "std"

                if($stdinfo)
                {
                    $CreatedSTD = 1
                    $SumofProducts++
                }
            } 

            if($CreateLTD)
            {
                Write-Host "Generating Long Term Disablility Product"
                #Comment this AutoGen Endpoint has not been created yet
                $jsonbody = ""
                $ltdinfo = Set-StaticProduct $AdminURI $CaseGUID $mysession "ltd"

                if($ltdinfo)
                {
                    $CreatedLTD = 1
                    $SumofProducts++
                }
            } 
        }

        if($medicalinfo.planID)
        {
            if($UseAutoGen)
            {
                Write-Host "AutoGen Employee Creation Loop"
                $hashtable = @()
                for($x=1; $x -le $NumberOfUsersPerCase; $x++)
                {
                    $emp = Invoke-RestMethod -URI $adminuri/api/cases/$($CaseGUID)/employees/autogen?forPickupOnly=true -Method POST -WebSession $mysession
                    $hashtable += [PSCustomObject] @{
                        "First Name" = $emp.firstName ;
                        "Last Name" = $emp.LastName;
                        "Birth Date" = get-date $($emp.dateofbirth) -Format "MM/dd/yyyy";
                        "Employee ID" = $emp.employeeID;
                        "SSN" = $emp.socialSecurityNumber;
                        "Address 1" = $emp.Address;
                        "City" = $emp.city;
                        "Hire Date" = get-date $($emp.dateofHire) -Format "MM/dd/yyyy";
                        "Zip" = $emp.zip;
                        "hasMedical" = $CreatedMedical;
                        "hasDental" = $CreatedDental;
                        "hasVision" = $CreatedVision;
                        "hasSLADD" = $CreatedSLADD; 
                        "hasBLADD" = $CreatedBLADD;
                        "hasSTD" = $CreatedSTD;
                        "hasLTD" = $CreatedLTD   #Add new products here
                    }
                }
            }
            else
            {
                Write-Host "Calling python script for emp generation"
                $output = python ./GenerateCSV.py -n $NumberOfUsersPerCase -f "./Emps-$($CaseGUID).csv"
                Write-Host $output

                $Emps = Import-Csv "./Emps-$($CaseGUID).csv"
                $hashtable = $Emps | select "First Name", 
                "Last Name", 
                "Birth Date", 
                "Employee ID", 
                "SSN",
                "Address 1",
                "City",
                "Hire Date",
                "Zip",
                @{Name="SumOfProducts"; Expression = {$SumofProducts}},
                @{Name="hasMedical"; Expression = {$CreatedMedical}},
                @{Name="hasDental"; Expression = {$CreatedDental}},
                @{Name="hasVision"; Expression = {$CreatedVision}},
                @{Name="hasSLADD"; Expression = {$CreatedSLADD}},
                @{Name="hasBLADD"; Expression = {$CreatedBLADD}},
                @{Name="hasSTD"; Expression = {$CreatedSTD}},
                @{Name="hasLTD"; Expression = {$CreatedLTD}} #Add new products here
            }

            Write-Host "Launching Case"
            $null = Invoke-RestMethod -Method POST -URI $adminuri/api/cases/$CommunityGUID/launch -WebSession $mysession
            
            Write-Host "Uploading Cencus to Choice Admin"
            $CensusFilePath = $(Get-ChildItem ./Emps-$($CaseGUID).csv).FullName
            $CensusGUID = Set-Census $AdminURI $CommunityGUID $mysession $CensusFilePath
            
            Write-Host "Checking if Case has lunched"
            $caseinfo = Invoke-RestMethod -uri $adminuri/api/cases/$CommunityGUID -Method GET -WebSession $mysession
            while (!$caseinfo.isLaunched)
            {
                Write-host "Waiting for Case to Launch"
                Start-Sleep 5
                $caseinfo = Invoke-RestMethod -uri $adminuri/api/cases/$CommunityGUID -Method GET -WebSession $mysession
            }
            $LaunchedCase = 1
        }
    }
    else
    {
        Write-Host "FAILED: Generating Case"    
    }
    
    $pstable = [PSCustomObject] [ordered] @{
        "Brokerage" = $brokerage
        "CreatedUser" = $CreatedUser;
        "EscalatedPrivs" = $EscalatedPrivs;
        "CaseName" = $casename;
        "CaseCommunityGUID" = $CommunityGUID;
        "CaseGuid" = $CaseGUID;
        "CensusGUID" = $CensusGUID;
        "EmpCSVFile" = "Emps-$($CaseGUID).csv";
        "SumOfProducts" = $SumofProducts;
        "hasMedical" = $medicalinfo.planID;
        "hasDental" = $dentalinfo.planID;
        "hasVision" = $visioninfo.planID;
        "hasSLADD" = $SLADDinfo.guid;
        "hasBLADD" = $BLADDinfo.guid;
        "hasSTD" = $stdinfo.guid;
        "hasLTD" = $ltdinfo.guid;
        "NumberOfEmps" = $hashtable.count;
        "employeeData" = $hashtable;
        "CaseLaunched" = $LaunchedCase
    }

    return $pstable
}

