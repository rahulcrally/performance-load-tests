function Run-SqlQuery($database, $query) {
    return Invoke-SqlCmd -ServerInstance $env:DB_HOST -Database $database -Username $env:DB_USER -Password $env:DB_PWD -Query $query
}

function Invoke-AccountsRequest([string]$Uri, [string]$Method, $Body) {
    $pset = @{
        Uri = $Uri
        Method = $Method
        ContentType = 'application/json'
        DisableKeepAlive = $true
    }

    if ($body -ne $null) {
        $pset.Body = (ConvertTo-Json $Body)
    }
    Write-Host $pset.Body
    Invoke-RestMethod @pset
}

Push-Location
$accountsBaseUrl = $env:ACCOUNTS_BASE_URL

if (-not $accountsBaseUrl) {
    $accountsBaseUrl = 'https://accounts.load-test.werally.in'
}

# Create a user in accounts and get rally ID
$env:LOAD_TEST_USER_NAME = "$([guid]::NewGuid())@mailinator.com".Replace('-', '')
Write-Host "##teamcity[setParameter name='env.LOAD_TEST_USER_NAME' value='$env:LOAD_TEST_USER_NAME']"
$loginPayload = @{
    "email"= $env:LOAD_TEST_USER_NAME;
    "confirmEmail"= $env:LOAD_TEST_USER_NAME;
    "password"= "Testing123!"
    "confirmPassword"= "Testing123!";
    "acceptedToS"= $True;
    "partner"= "choiceAdmin";
    "inviteCode"= $null;
    "clientUrlSlug"= $null;
    "ssoToken"= $null;
    "credentials"= $null;
    "product"= "choiceAdmin";
    "acceptedHipaa"= $False;
    # "legalAgreements"=@{
    #     "name"= "TOS-ChoiceAdmin";
    #     "version"= "08.01.2016";
    # };
    "eligFields"= @();
    "locale"= "en-US";
}

$loginResponse = Invoke-AccountsRequest "$accountsBaseUrl/auth/v1/register" 'POST' $loginPayload
Write-Host $loginResponse

$infoPayload = @{"authToken"= $loginResponse.authToken}

$infoResponse = Invoke-AccountsRequest "$accountsBaseUrl/auth/v1/getUserInfo" 'POST' $infoPayload
Write-Host $infoResponse
$env:LOAD_TEST_USER_ID = $infoResponse.rallyId

# Get identity provider id
$identityProviderId = Run-SqlQuery Spotlite "
    SELECT IdentityProviderId 
    FROM fedgrant.IdentityProviders 
    WHERE LookupValue = 'auth.werally.in'
"
if (-not $identityProviderId) {
    Write-Host "No matching identity provider for auth.werally.in" 
    Exit(-1)
}
$identityProviderId = $identityProviderId.Item(0)

# Check that user already exists
$userQuery = Run-SqlQuery Spotlite "
    SELECT LoginId FROM fedgrant.Logins WHERE UPN = '$env:LOAD_TEST_USER_ID' AND IdentityProviderId = $identityProviderId 
"

if ($userQuery) {
    Write-Host "User already exists auth.werally.in`:$env:LOAD_TEST_USER_ID"
    Exit(-1)
}

# Get payer id
$queryResponse = Run-SqlQuery Spotlite "
    SELECT Identifier FROM Payers WHERE Name = 'uhc'
    "

if (-not $queryResponse) {
    Write-Host "No payer with name uhc"
    Exit(-1)
}

$payerId = $queryResponse.Item(0)

# Insert Spotlite fedgrant
Run-SqlQuery Spotlite "
    INSERT INTO fedgrant.Logins
               (Upn
               ,IdentityProviderId
               ,Email
               ,FirstName
               ,LastName)
         VALUES
               ('$env:LOAD_TEST_USER_ID'
               ,$identityProviderId
               ,'$env:LOAD_TEST_USER_NAME'
               ,'Load'
               ,'Tester')
               "

# Get new LoginId
$queryResponse = Run-SqlQuery Spotlite "
    SELECT LoginId FROM fedgrant.Logins WHERE Upn = '$env:LOAD_TEST_USER_ID'
    "

$newLoginid = $queryResponse.Item(0)

#### Create permissions for access as a UHC payer
# Get Payer RoleId
$roleResponse = Run-SqlQuery Spotlite "
    SELECT RoleId, Identifier FROM fedgrant.Roles WHERE Name = 'payer admin'
    "
$roleId = $roleResponse['RoleId'] 

# Create Payer Login Role
Run-SqlQuery Spotlite "
    INSERT INTO fedgrant.LoginRoles
            (LoginId
            ,RoleId)
        VALUES
            ($newLoginid
            ,$roleId)
"

# Get LoginRoleId
$queryResponse = Run-SqlQuery Spotlite "
    SELECT LoginRoleId FROM fedgrant.LoginRoles WHERE LoginId = $newLoginid AND RoleId = $roleId
    "

$loginRoleId = $queryResponse.Item(0)

# Get payer allowance type id
$queryResponse = Run-SqlQuery Spotlite "
    SELECT AllowanceTypeId FROM fedgrant.AllowanceTypes WHERE LookupValue = 'Payer'
    "

$payerAllowanceTypeId = $queryResponse.Item(0)

# Create allowance
Run-SqlQuery Spotlite "
    INSERT INTO fedgrant.Allowances
            (LoginRoleId
            ,AllowanceTypeId
            ,AllowanceValue)
        VALUES
            ($loginRoleId
            ,$payerAllowanceTypeId
            ,'$payerId')
"

#### Create permissions for creating test brokerages
# Get Create Test Brokerage RoleId
$roleResponse = Run-SqlQuery Spotlite "
    SELECT RoleId, Identifier FROM fedgrant.Roles WHERE Name = 'Create Test Brokerage'
    "
$roleId = $roleResponse['RoleId'] 

# Create Create Test Brokerage Login Role
Run-SqlQuery Spotlite "
    INSERT INTO fedgrant.LoginRoles
            (LoginId
            ,RoleId)
        VALUES
            ($newLoginid
            ,$roleId)
"

# Get LoginRoleId
$queryResponse = Run-SqlQuery Spotlite "
    SELECT LoginRoleId FROM fedgrant.LoginRoles WHERE LoginId = $newLoginid AND RoleId = $roleId
    "

$loginRoleId = $queryResponse.Item(0)

# Get payer allowance type id
$queryResponse = Run-SqlQuery Spotlite "
    SELECT AllowanceTypeId FROM fedgrant.AllowanceTypes WHERE LookupValue = 'Global'
    "

$payerAllowanceTypeId = $queryResponse.Item(0)

# Create allowance
Run-SqlQuery Spotlite "
    INSERT INTO fedgrant.Allowances
            (LoginRoleId
            ,AllowanceTypeId)
        VALUES
            ($loginRoleId
            ,$payerAllowanceTypeId)
"

Write-Host "Created Load Test User $env:LOAD_TEST_USER_ID_PROVIDER`:$env:LOAD_TEST_USER_ID" 
Write-Host "##teamcity[setParameter name='env.LOAD_TEST_USER_ID' value='$env:LOAD_TEST_USER_ID']"

Pop-Location
# Write User info to file to be used by JMeter
$loadTestUserCsv = "AdminEmail,AdminPassword,AdminRallyId`n"
$loadTestUserCsv += "$env:LOAD_TEST_USER_NAME,$env:LOAD_TEST_USER_PASSWORD,$env:LOAD_TEST_USER_ID"

$scriptPath = $script:MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

write-Host "Creating File $dir\loadTestUser.csv"
$loadTestUserCsv | Out-File $dir\loadTestUser.csv -Encoding ascii