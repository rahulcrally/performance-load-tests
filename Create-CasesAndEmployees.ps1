Param(
    [int]    $NumberOfCases = 20,
    [int]    $NumberOfUsersPerCase = 500,
    [string] $environment =  "loadtest",
    [switch] $VerboseOutput,
    [switch] $UseLoLAuth = $false,
    [switch] $UseAutoGen = $false,
    [string] $RealAuth = "https://accounts.load-test.werally.in",
    [int]    $NumberOfThreads = $NumberOfCases+1,
    [int]    $MedicalPercentage = 100,
    [int]    $DentalPercentage = 90,
    [int]    $VisionPercentage = 80,
    [int]    $BLADDPercentage = 80,
    [int]    $SLADDPercentage = 80,
    [int]    $STDPercentage = 60,
    [int]    $LTDPercentage = 80
)

$randomuser = "$(Get-Random)@rallyhealth.com"
$AdminURI = "https://choiceadmin.$environment.rallytesting.net"
$LoLAuth = "https://account.$environment.rallytesting.net"
$emu = "https://emu.$environment.rallytesting.net"
$scriptPath = $script:MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

if($VerboseOutput)
{
    Write-Host "variables--------------------------------------"
    Write-Host "NumberOfThreads: $NumberOfThreads"
    Write-Host "NumberOfUsersPerCase: $NumberOfUsersPerCase"
    Write-Host "VerboseOutput: $VerboseOutput"
    Write-Host "Brokerage GUID: $brokerage"
    Write-Host "Environment: $environment"
    Write-Host "Choice Admin UserName: $randomuser"
    Write-Host "Choice Admin Password: $Password"
    Write-Host "Use LoLAuth: $UseLoLAuth"
    Write-Host "LoLAuth URI: $LoLAuth"
    Write-Host "Real Auth URI: $RealAuth"
    Write-Host "Choice Admin URI: $AdminURI"
    Write-Host "Shortcut Employee Creation: $PickUpOnly"
    Write-Host "Emu Url: $emu"
    Write-Host "Current Directory: $dir"        
    Write-Host "----------------------------------------------"
}

$starttime = Get-Date
Write-Host "Start Script"
Write-Host "Creating new Brokerage"

#generating a random password, because there is no need for a static choiceadmin password
for($l=97; $l -lt 123; $l++)
{
    $asciiLower += ,[char][byte] $l
}
for($l=65; $l -lt 91; $l++)
{
    $asciiUpper += ,[char][byte] $l
}
for($l=48; $l -lt 65; $l++)
{
    $asciiOther += ,[char][byte] $l
}
for($l=0; $l -lt 4; $l++)
{
    $Password += ($asciiLower | Get-Random)
    $Password += ($asciiUpper | Get-Random)
    $Password += ($asciiOther | Get-Random)
}
$Password += "1!"

if($UseLoLAuth)
{
    Write-Host "Registering LoLAuth User"
    $body = @{
        "email"="$randomuser";
        "password" = "$Password"
    } | convertto-json

    $CreateUser = invoke-restmethod -uri "$LoLAuth/accountsstub/emaillogin" -body $body -method POST -ContentType "application/json" -Session mysession
    $auth = $CreateUser.AuthenticationToken
}
else 
{
    $loginBody = @{
        "email"= "$randomuser";
        "confirmEmail"= "$randomuser";
        "password"= "$Password";
        "confirmPassword"= "$Password";
        "acceptedToS"= $True;
        "partner"= "choiceAdmin";
        "inviteCode"= $null;
        "clientUrlSlug"= $null;
        "ssoToken"= $null;
        "credentials"= $null;
        "product"= "choiceAdmin";
        "acceptedHipaa"= $False;
        "eligFields"= @();
        "locale"= "en-US";
    } | ConvertTo-Json

    Write-Host "Real Auth Creating User"
    $loginResponse = Invoke-RestMethod `
    -URI "$realAuth/auth/v1/register" `
    -Method POST `
    -Body $loginBody `
    -Session mysession `
    -ContentType 'application/json' 
    
    $auth = $loginResponse.authToken
    
    Write-Host "Getting Rally ID"
    $infoPayload = @{"authToken"= $auth} | ConvertTo-Json
    $infoResponse = Invoke-RestMethod `
    -URI "$realAuth/auth/v1/getUserInfo" `
    -Method POST `
    -body $infoPayload `
    -WebSession $mysession `
    -ContentType "application/json"

    $rallyID =  $infoResponse.rallyid
}
if($auth)
{
    Write-Host "Authing with Choice Admin"
    $ChoiceAdminResponse = Invoke-RestMethod -uri $adminuri/?a=$auth -method GET -WebSession $mysession

    Write-Host "Escalating Privs"
    $EscaltingPrivs = Invoke-RestMethod -uri $emu/nonprod/escalateprivileges -method GET -WebSession $mysession

    $BrokerageBody = @{
        name = "Performance Test $(get-date)";
        address = "20 N Wacker St";
        city = "Chicago";
        statecode = "IL" ;
        zip = "60606";
        payerguid = "32d4c5c6-08d9-4251-b112-06549419552b";
        isTestBrokerage = "true"
    } | ConvertTo-Json

    $BrokerageResp = Invoke-RestMethod -uri $adminuri/api/brokerages/ -Method POST -Body $BrokerageBody -WebSession $mysession -ContentType "application/json"
    $brokerage = $BrokerageResp.guid
}

if($brokerage)
{
    Write-Host "Successfully Created Brokerage: $brokerage"
    Write-Host "Spinning up Threads for Creation of Case, Product and Employees"


    $pool = [RunspaceFactory]::CreateRunspacePool(1, $NumberOfThreads)
    $pool.Open()
    $runspaces = @()
    $returnData = @()
    
    #Jobs was running slow so I used Runspace, which was almost 50% faster
    for($x=1; $x -le $NumberOfCases; $x++)
    {
        $CreateMedical = $false
        $CreateVision = $false
        $CreateDental = $false
        $CreateSLADD = $false
        $CreateBLADD = $false
        $CreateSTD = $false
        $CreateLTD = $false
        
        if($x -le $NumberOfCases*($MedicalPercentage/100))
        {
            $CreateMedical = $true
        }

        if($x -le $NumberOfCases*($DentalPercentage/100))
        {
            Write-Debug "Create Dental"
            $CreateDental = $true
        }

        if($x -le $NumberOfCases*($VisionPercentage/100))
        {
            Write-Debug "Create Vision"
            $CreateVision = $true
        }
        
        if($x -le $NumberOfCases*($SLADDPercentage/100))
        {
            Write-Debug "Create SLADD"
            $CreateSLADD = $true
        }
        
        if($x -le $NumberOfCases*($BLADDPercentage/100))
        {
            Write-Debug "Create BLADD"
            $CreateBLADD = $true
        }
        
        if($x -le $NumberOfCases*($STDPercentage/100))
        {
            Write-Debug "Create STD"
            $CreateSTD = $true
        }

        if($x -le $NumberOfCases*($LTDPercentage/100))
        {
            Write-Debug "Create LTD"
            $CreateLTD = $true
        }

        $runspace = [PowerShell]::Create()
        
        $runspace.AddScript({
            param(
                [Parameter(Position=1)]
                [string] $ScriptName,
                [Parameter(Position=2)]
                $Session,
                [Parameter(Position=3)]
                [switch] $EnableJobOutput,
                [Parameter(Position=4)]
                [int]    $NumberOfUsersPerCase,
                [Parameter(Position=5)]
                [string] $environment,
                [Parameter(Position=6)]
                [switch] $UseAutoGen,
                [Parameter(Position=7)]
                [switch] $UseLoLAuth,
                [Parameter(Position=8)]
                [string] $brokerage,
                [Parameter(Position=9)]
                [string] $RealAuthURI,
                [Parameter(Position=10)]
                [switch] $CreateMedical,
                [Parameter(Position=11)]
                [switch] $CreateVision,
                [Parameter(Position=12)]
                [switch] $CreateDental,
                [Parameter(Position=13)]
                [switch] $CreateBLADD,
                [Parameter(Position=14)]
                [switch] $CreateSLADD,
                [Parameter(Position=15)]
                [switch] $CreateSTD,
                [Parameter(Position=16)]
                [switch] $CreateLTD
            )
            & $ScriptName $Session $EnableJobOutput $NumberOfUsersPerCase $environment $UseAutoGen $UseLoLAuth $brokerage $RealAuthURI $CreateMedical $CreateVision $CreateDental $CreateBLADD $CreateSLADD $CreateSTD $CreateLTD
        }) | Out-Null

        $runspace.AddArgument($(get-childitem ./Create-LoadData.ps1).FullName) | Out-Null
        $runspace.AddArgument($mysession) | Out-Null
        $runspace.AddArgument($VerboseOutput) | Out-Null
        $runspace.AddArgument($NumberOfUsersPerCase) | Out-Null
        $runspace.AddArgument($environment) | Out-Null
        $runspace.AddArgument($UseAutoGen) | Out-Null
        $runspace.AddArgument($UseLoLAuth) | Out-Null
        $runspace.AddArgument($brokerage) | Out-Null
        $runspace.AddArgument($realAuth) | Out-Null
        $runspace.AddArgument($CreateMedical) | Out-Null
        $runspace.AddArgument($CreateVision) | Out-Null
        $runspace.AddArgument($CreateDental) | Out-Null
        $runspace.AddArgument($CreateBLADD) | Out-Null
        $runspace.AddArgument($CreateSLADD) | Out-Null
        $runspace.AddArgument($CreateSTD) | Out-Null
        $runspace.AddArgument($CreateLTD) | Out-Null
        
        $runspace.RunspacePool = $pool
        $runspaces += [PSCustomObject]@{Pipe = $runspace; Status = $runspace.BeginInvoke()}
    }

    while($runspaces.status.iscompleted | where {$_ -ne $true})
    {
        $StillWaiting = $runspaces.status.iscompleted | where {$_ -ne $true}
        write-host "Waiting for $($StillWaiting.count) runspaces to finish"
        start-sleep 20
    }
    
    foreach ($thread in $runspaces)
    {
        $returnData += $thread.Pipe.EndInvoke($thread.status)
    }

    $endtime = get-date

    $returnData = $returnData | Where-Object {$_.gettype() -ne [string]} #doing this to filter out the jobstatus that gets returned
    $LogStatus = $returnData | Select-Object * -ExcludeProperty employeeData,RunspaceID,PSSourceJobInstanceId #remove out employeeData
    if($VerboseOutput)
    {
        $LogStatus
    }
    else 
    {
        $LogStatus | Export-Csv "$($MyInvocation.MyCommand.Name)Log.csv" -NoTypeInformation -Encoding ASCII
    }
    $empcounter = 0
    $LogStatus | ForEach-Object {$empcounter += [int] $_.NumberOfEmps}
    
    $casecounter = @($LogStatus | Where-Object {$_.CaseLaunched -eq 1})
    $MedicalCounter = @($LogStatus | Where-Object {$_.hasMedical -ne $null})
    $DentalCounter = @($LogStatus | Where-Object {$_.hasDental -ne $null})
    $VisionCounter = @($LogStatus | Where-Object {$_.hasVision -ne $null})
    $BLADDCounter = @($LogStatus | Where-Object {$_.hasBLADD -ne $null})
    $SLADDCounter = @($LogStatus | Where-Object {$_.hasSLADD -ne $null})
    $STDCounter = @($LogStatus | Where-Object {$_.hasSTD -ne $null})
    $LTDCounter = @($LogStatus | Where-Object {$_.hasLTD -ne $null})
    $casefailedcount = @($LogStatus | Where-Object {$_.CaseLaunched -eq 0})
    

    Write-host "<Job Summary----------------------------------------------------------->"
    Write-Host "Cases Launched  : $($casecounter.count)"
    Write-Host "Total Employees : $empcounter"
    Write-Host "Medical Count   : $($MedicalCounter.count)"
    Write-Host "Vision Count    : $($VisionCounter.count)"
    Write-Host "Dental Count    : $($DentalCounter.count)"
    Write-Host "BLADD Count     : $($BLADDCounter.count)"
    Write-Host "SLADD Count     : $($SLADDCounter.count)"
    Write-Host "STD Count       : $($STDCounter.count)"
    Write-Host "LTD Count       : $($LTDCounter.count)"
    Write-Host "FAILED Cases    : $($casefailedcount.count)"

    Write-Host ""
    Write-host "<Export Summary----------------------------------------------------------->"
    
    Write-Host "Exporting Employee Data to GenPerfTest_Users.csv" 
    $employeedata = $returnData.employeeData
    $employeedata = $employeedata | Sort-Object {Get-Random}
    $employeedata | Export-Csv $dir\GenPerfTest_Users.csv -Encoding ascii -NoTypeInformation -Force

    Write-Host "Exporting Broker Data to PerfTest_Brokerages.csv"
    $brokerage | select @{Name="BrokerGUID"; Expression = {$_}} | Export-Csv $dir\PerfTest_Brokerages.csv -Force -Encoding ascii -NoTypeInformation

    Write-Host "Exporting Case Data to GenPerfTest_Cases.csv"
    $returnData | select @{Name="CaseGUID"; Expression = {$_.CaseCommunityGUID}}, @{Name="PerfUsersCSV"; Expression = {$_.EmpCSVFile}}  | Export-Csv $dir\GenPerfTest_Cases.csv -Force -NoTypeInformation -Encoding ascii

    Write-Host "Exporting Admin User"
    $AdminCreds = [PSCustomObject] @{
        AdminEmail = $randomuser;
        AdminPassword = $Password;
        AdminRallyId = $rallyID
    }


    write-Host "Creating File $dir\loadTestUser.csv"
    $AdminCreds | Export-Csv $dir\loadTestUser.csv -Encoding ascii -NoTypeInformation -Force

    Write-Host ""
    Write-host "<Runtime Summary---------------------------------------------------------->"
    $($endtime - $starttime) | Out-Host #To Insure this doesn't go into the Output Stream.

}
