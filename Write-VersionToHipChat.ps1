param(
    $HipChatAPIKey,
    $OctopusAPIKey
)

#determine if this is windows, encase you're cool and want to run on powershell core
if($IsWindows -eq $null)
{
    $IsWindows = test-path C:\
}

#PS core has no issues with Cert Trust
if($IsWindows)
{
#this can not be tabbed in, parser becomes unhappy
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    } 
"@
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 #enable TLS1.2 for new HipChat
}

$OctopusHeaders = @{"X-Octopus-ApiKey" =  "$OctopusAPIKey"}

$enviroments = Invoke-RestMethod -uri https://octopus.rallytools.net/api/environments/ -Headers $OctopusHeaders
$loadtestID = $($enviroments.items | where {$_.name -eq "loadtest"}).ID
$productionID = $($enviroments.items | where {$_.name -eq "production"}).ID

if($loadtestID)
{
    $Projects = Invoke-RestMethod -uri "https://octopus.rallytools.net/api/projects?take=1000" -Headers $OctopusHeaders 
    $ProjectsHashed = $projects.items | Group-Object "ID" -AsHashTable 
    if($ProjectsHashed)
    {
        $OctoDash = Invoke-RestMethod -uri "https://octopus.rallytools.net/api/dashboard/dynamic?" -Headers $OctopusHeaders
        $mydeploy = $OctoDash.items | where {$_.EnvironmentID -eq $loadtestID}
        $proddeploy = $OctoDash.items | where {$_.EnvironmentID -eq $productionID}
    }

    $ComparedProjects = Compare-Object -ReferenceObject $mydeploy -DifferenceObject $proddeploy -Property ReleaseVersion,ProjectID
    $differentProjects = $ComparedProjects | Select-Object ProjectID -Unique -ExpandProperty ProjectID

    if($differentProjects.count -gt 0)
    {
        $CleanTable = @()
        $MessageHTML = "<b>Load Test Version info</b>`n"
        $MessageHTML += "<table>`n"
        $MessageHTML += "<tr>`n"
        $MessageHTML += "<th>Application Name----------------</th>`n"
        $MessageHTML += "<th>Production Version--------------</th>`n"
        $MessageHTML += "<th>LoadTest Version</th>`n"
        $MessageHTML += "</tr>`n"

        ForEach($project in $differentProjects)
        {
            $name = $($ProjectsHashed[$($project)]).Name
            $prod = $($ComparedProjects | where{$_.projectid -eq $project -and $_.SideIndicator -eq "=>"}).ReleaseVersion
            $load = $($ComparedProjects | where{$_.projectid -eq $project -and $_.SideIndicator -eq "<="}).ReleaseVersion

            $row = @{
                "Application Name" = $name;
                "Production Version" = $prod;
                "LoadTest Version" = $load
            }
            $pstable = New-Object -TypeName psobject -Property $row
            $CleanTable += $pstable

            $MessageHTML += "<tr>`n"
            $MessageHTML += "<td>$name</td>`n"
            $MessageHTML += "<td>$prod</td>`n"
            $MessageHTML += "<td>$load</td>`n"
            $MessageHTML += "</tr>`n"
        }
        $MessageHTML += "</table>`n"
    }
    else
    {
        $MessageHTML = "<b>Load Test All Versions are the Same as Prodution</b>`n"
    }

    $hipchatPayload = @{
        "color" ="purple";
        "message" = "$MessageHTML";
        "notify" ="false";
        "message_format" = "html"
    }

    Invoke-RestMethod -Method POST -Uri "https://hipchat.rallyhealth.com/v2/room/312/notification?auth_token=$HipChatAPIKey" -Body $hipchatPayload

    exit 0
}
else
{
    Write-Host "FAILED: Getting data from Octopus"
    exit 1
}

